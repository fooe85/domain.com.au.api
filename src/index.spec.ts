import { ListingType, PropertyTypes, SCOPES } from './enum';
import { BaseClient } from './index';
describe("test", () => {
    const client = new BaseClient(process.env.DOMAIN_API_CLIENT_ID, process.env.DOMAIN_API_CLIENT_SECRET);
    it("Should successful get token", async () => {
        
        const token = await client.requestToken([SCOPES.LISTING_READ]);
        expect(token).toEqual({
            access_token: expect.any(String),
            expires_in: expect.any(Number),
            token_type: "Bearer"
        });

    });
    it('agenciesSearch() should works', async () => {

        const data = await client.agenciesSearch({ q: 'Pyrmont' });
        expect(data.result).toMatchSnapshot();
    })

    it('getAgencyById() should works', async () => {

        const data = await client.getAgencyById(34024);
        expect(data.result).toMatchSnapshot();
    })

    it('GetAgent() should works', async () => {
        const data = await client.getAgent('1394623');
        expect(data.result).toMatchSnapshot();
    });
    it('agentSearch() should works', async () => {
        const data = await client.agentSearch({query: "truong"});
        expect(data.result.length).toBeGreaterThan(0);
    })

    it('getListingByAgent() should works', async () => {
        const data = await client.getListingByAgentId(1394623);
        expect(data.result.length).toBeGreaterThan(0);
    })
    it('getListingByAgencyId() should works', async () => {
        const data = await client.getListingByAgencyId(34024);
        expect(data.result).toMatchSnapshot();
    })

    it('listingLocation() should works', async () => {

        const data = await client.listingLocation({ terms: 'Derrimut' });
        expect(data.result).toMatchSnapshot();
    })

    it('getListingById() should works', async () => {

        const data = await client.getListingById(2016564278);
        expect(data.result).toMatchSnapshot();
    })

    it("Residential search should successful", async () => {
        
        const data = await client.searchListing(
            {
                "listingType": ListingType.Sale,
                "propertyTypes":[
                  PropertyTypes.House,
                ],
                "minBedrooms":3,
                "minBathrooms":2,
                "minCarspaces":1,
                "locations":[
                  {
                    "state":"VIC",
                    "region":"",
                    "area":"",
                    "suburb":"Derrimut",
                    "postCode":"",
                    "includeSurroundingSuburbs":false
                  }
                ]
              }
              
        );
        expect(data.result.length).toBeGreaterThan(0)
        //console.log(data.result)
    })

})
