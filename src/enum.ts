export enum SCOPES {
  LISTING_READ = 'api_listings_read',
  AGENCY_READ = "api_agencies_read"
}
export enum EndPoints {
  LISTINGS_RESIDENTIAL_SEARCH = "/v1/listings/residential/_search",
  LISTINGS_LOCATION = "/v1/listings/locations",
  LISTINGS = "/v1/listings/{id}",
  AGENCIES = "/v1/agencies",
  AGENCY_BY_ID = "/v1/agencies/{id}/",
  AGENCY_LISTINGS = "/v1/agencies/{id}/listings/",
  AGENT_BY_ID = "/v1/agents/{id}/",
  AGENT_LISTINGS = "/v1/agents/{id}/listings/",
  AGENTS_SEARCH = "/v1/agents/search/"
}
  export enum PropertyTypes {
    House = "House"
  }
  export enum ListingType {
    Sale = "Sale"
  }
