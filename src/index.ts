import { EndPoints, SCOPES } from "./enum";
import Axios from "axios";
import * as qs from "qs";
import { ScopeMappings } from './scope-mapping';
import { ListingSearchRequest, PageRequest, ListingLocationRequest, AgencySearchRequest, AgentSearchRequest } from './model';
import { AgencyDetails, Agency } from './models/agencies';
import { ListingDetails } from './models/listings';
import { Agent } from './models/agents';

export class BaseClient {
    private token: {access_token:string};
    private currentScopes: SCOPES[];
    constructor(private clientId, private clientSecret, public scopes: SCOPES[] = []) {
        this.currentScopes = scopes;
    }
    async requestToken(scopes?: SCOPES[]) {
        let shouldRefreshToken = false;

        scopes.forEach(scope => {
            if (!this.currentScopes.includes(scope)) {
                shouldRefreshToken = true;
                this.currentScopes.push(scope)
            }
        });
        
        if (this.currentScopes.length === 0) {
            throw new Error('Invalid scope');
        }
        if (!shouldRefreshToken && this.token) {
            return this.token;
        }

        try {
            const token = await Axios.post('https://auth.domain.com.au/v1/connect/token', qs.stringify({
                scope: this.currentScopes.join(' '),
                grant_type: 'client_credentials'
            }),
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    auth: {
                        username: this.clientId,
                        password: this.clientSecret
                    }
                
                })
            this.token = token.data;
            return this.token
        }
        catch (err) {
            console.log(err)
        }
    }
    
    extractMeta(headers) {
        return {
            'x-quota-persecond-limit': ++headers['x-quota-persecond-limit'],
            'x-quota-persecond-remaining': ++headers['x-quota-persecond-remaining'],
            'x-quota-perminute-limit': ++headers['x-quota-perminute-limit'],
            'x-quota-perminute-remaining': ++headers['x-quota-perminute-remaining'],
            'x-quota-perday-limit': ++headers['x-quota-perday-limit'],
            'x-quota-perday-remaining': ++headers['x-quota-perday-remaining'],
            'x-pagination-pagenumber': +headers['x-pagination-pagenumber'],
            'x-pagination-pagesize': +headers['x-pagination-pagesize'],
            'x-total-count': +headers['x-total-count'],
        };
    }

    async get<T>(endpoint: EndPoints, query?: PageRequest) {
        const scopes = ScopeMappings[endpoint];
        const token = await this.requestToken(scopes);
        let url = `https://api.domain.com.au${endpoint}`;
        Object.entries(query || {}).forEach(([key, value]) => {
            url = url.replace("{" + key + "}", value);
        })
        const requestOptions = {
            headers: {
                authorization: `Bearer ${token.access_token}`,
                'content-type': 'application/json'
            }
        };
        const res = await Axios.get(url + "?" + qs.stringify(query), requestOptions);

        return {
            meta: this.extractMeta(res.headers),
            result: res.data as T
        };
    }

    async post<T, TResult>(endpoint: EndPoints, data: T, page?: PageRequest   ) {
        const scopes = ScopeMappings[endpoint];
        const token = await this.requestToken(scopes);
        const url = `https://api.domain.com.au${endpoint}`;
        const requestOptions = {
            headers: {
                authorization: `Bearer ${token.access_token}`,
                'content-type': 'application/json'
            }
        };
        const res = await Axios.post(url, {...data, ...(page || {})}, requestOptions);

        return {
            meta: this.extractMeta(res.headers),
            result: res.data as TResult
        };
    }
    
    async listingLocation(request: ListingLocationRequest) {
        return this.get(EndPoints.LISTINGS_LOCATION, request);
    }
    
    async agenciesSearch(request: AgencySearchRequest) {
        return this.get<Agency[]>(EndPoints.AGENCIES, request);
    }

    async getAgencyById(id: number) {
        return this.get<AgencyDetails>(EndPoints.AGENCY_BY_ID, {id} as PageRequest);
    }

    async searchListing(request: ListingSearchRequest, pageNumber =1, pageSize = 100) {
        return this.post<ListingSearchRequest, any>(EndPoints.LISTINGS_RESIDENTIAL_SEARCH, request, { pageNumber, pageSize })
    }

    async getListingById(id: number) {
        return this.get(EndPoints.LISTINGS, {id} as PageRequest);
    }
    async getListingByAgencyId(id: number) {
        return this.get<ListingDetails[]>(EndPoints.AGENCY_LISTINGS, {id} as PageRequest);
    }

    async getAgent(id: string) {
        return this.get<Agent>(EndPoints.AGENT_BY_ID, {id} as PageRequest);
    }
    async getListingByAgentId(id: number) {
        return this.get<ListingDetails[]>(EndPoints.AGENT_LISTINGS, {id} as PageRequest);
    }

    async agentSearch(request: AgentSearchRequest) {
        return this.get<Agent[]>(EndPoints.AGENTS_SEARCH, request);
    }

}
