export interface ListingDetails {
  objective?:             string;
  propertyTypes?:         string[];
  status?:                string;
  saleMode?:              string;
  channel?:               string;
  addressParts?:          AddressParts;
  advertiserIdentifiers?: AdvertiserIdentifiers;
  apmIdentifiers?:        ApmIdentifiers;
  bathrooms?:             number;
  bedrooms?:              number;
  carspaces?:             number;
  dateAvailable?:         Date;
  dateUpdated?:           Date;
  description?:           string;
  geoLocation?:           GeoLocation;
  headline?:              string;
  id?:                    number;
  isNewDevelopment?:      boolean;
  media?:                 Media[];
  priceDetails?:          PriceDetails;
  propertyId?:            string;
  rentalDetails?:         RentalDetails;
  seoUrl?:                string;
  virtualTourUrl?:        string;
}

export interface AddressParts {
  stateAbbreviation?: string;
  displayType?:       string;
  streetNumber?:      string;
  unitNumber?:        string;
  street?:            string;
  suburb?:            string;
  postcode?:          string;
  displayAddress?:    string;
}

export interface AdvertiserIdentifiers {
  advertiserType?: string;
  advertiserId?:   number;
  contactIds?:     number[];
}

export interface ApmIdentifiers {
  suburbId?: number;
}

export interface GeoLocation {
  latitude?:  number;
  longitude?: number;
}

export interface Media {
  category?: string;
  type?:     string;
  url?:      string;
}

export interface PriceDetails {
  canDisplayPrice?: boolean;
  displayPrice?:    string;
}

export interface RentalDetails {
  rentalMethod?:    string;
  source?:          string;
  canDisplayPrice?: boolean;
}
