export interface AgencyDetails {
  accountType?:    string;
  profile?:        Profile;
  dateUpdated?:    Date;
  name?:           string;
  details?:        Details;
  id?:             number;
  agents?:         Agent[];
  contactDetails?: ContactDetails;
}
export interface Agency {
  inSuburb?:            boolean;
  hasRecentlySold?:     boolean;
  id?:                  number;
  name?:                string;
  suburb?:              string;
  address1?:            string;
  address2?:            string;
  telephone?:           string;
  rentalTelephone?:     string;
  mobile?:              string;
  fax?:                 string;
  state?:               string;
  description?:         string;
  email?:               string;
  rentalEmail?:         string;
  accountType?:         number;
  numberForSale?:       number;
  numberForRent?:       number;
  domainUrl?:           string;
  showTabSoldLastYear?: boolean;
}

export interface Agent {
  agencyId?:           number;
  id?:                 number;
  email?:              string;
  firstName?:          string;
  mobile?:             string;
  photo?:              string;
  lastName?:           string;
  phone?:              string;
  secondaryEmail?:     string;
  facebookUrl?:        string;
  twitterUrl?:         string;
  agentVideo?:         string;
  profileText?:        string;
  googlePlusUrl?:      string;
  personalWebsiteUrl?: string;
  linkedInUrl?:        string;
  contactTypeCode?:    number;
}

export interface ContactDetails {
  businessSale?:    BusinessSale;
  businessRent?:    BusinessRent;
  commercialLease?: BusinessRent;
  commercialSale?:  BusinessRent;
  emailDomains?:    EmailDomain[];
  general?:         General;
  residentialRent?: BusinessSale;
  residentialSale?: BusinessSale;
}

export interface BusinessRent {
}

export interface BusinessSale {
  email?: string;
  phone?: string;
}

export interface EmailDomain {
  domain?: string;
}

export interface General {
  email?:  string;
  fax?:    string;
  phone?:  string;
  mobile?: string;
}

export interface Details {
  streetAddress1?: string;
  streetAddress2?: string;
  suburb?:         string;
  state?:          string;
  postcode?:       string;
  agencyWebsite?:  string;
  principalName?:  string;
  principalEmail?: string;
}

export interface Profile {
  agencyPhotos?:            any[];
  profileWebsite?:          string;
  agencyBanner?:            string;
  agencyWebsite?:           string;
  agencyLogoStandard?:      string;
  agencyLogoSmall?:         string;
  logoColour?:              string;
  backgroundColour?:        string;
  mapLatitude?:             string;
  mapLongitude?:            string;
  mapCertainty?:            number;
  agencyVideoUrl?:          string;
  agencyDescription?:       string;
  numberForSale?:           number;
  numberForRent?:           number;
  numberForSaleCommercial?: number;
  numberForRentCommercial?: number;
}
